;; Copyright 2009, 2010 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Cluster
;;
;; CL-Cluster is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Cluster is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(cl:eval-when (:load-toplevel :execute)
  (asdf:operate 'asdf:load-op :bordeaux-threads)
  (asdf:operate 'asdf:load-op :split-sequence)
  (asdf:operate 'asdf:load-op :eager-future)
  (asdf:operate 'asdf:load-op :trivial-garbage)
  (asdf:operate 'asdf:load-op :zeromq))

(defpackage #:cl-cluster-asd
  (:use :cl :asdf))

(in-package #:cl-cluster-asd)

(defsystem cl-cluster
  :name "cl-cluster"
  :version "0.2"
  :author "Vitaly Mayatskikh <v.mayatskih@gmail.com>"
  :licence "GPLv3"
  :description "CL Cluster implementation"
  :serial t
  :components ((:file "package")
               (:file "cluster")
	       (:file "meta")
	       (:file "transport-ssh")
	       (:file "transport-zmq")))
