(in-package :cl-cluster)

(defmacro with-remote (node &body body)
  "Execute body at remote host."
  `(node-exec ,node (format nil "~s" '(progn ,@body))))

(defmacro send-remote (node &body body)
  "Execute body at remote host."
  `(node-exec ,node (format nil "~s" '(progn ,@body)) nil nil nil))

(defmacro exec-remote (node &body body)
  "Execute body at remote host."
  `(node-exec ,node (format nil "~s" '(progn ,@body)) nil t t))

(defmacro rplet (node (&rest bindings) &body body)
  "Let-like macro for remote lisp. Evaluate vars using pmap."
  (let ((syms (mapcar (lambda (x)
                        (gensym (string (car x))))
                      bindings)))
    `(node-exec ,node
		,(if (null syms) `(format nil "~s" '(progn ,@body))
		     `(format nil "(let ~s ~s)"
			      (let ,(loop for (nil exp) in bindings
				       for sym in syms
				       collect `(,sym (pexec ,exp)))
				(list ,@(loop for (var nil) in bindings
					   for sym in syms
					   collect `(list ',var (yield ,sym)))))
			      '(progn ,@body))))))
