;; Copyright 2009 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Cluster
;;
;; CL-Cluster is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Cluster is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:cl-cluster
  (:use :cl)
  (:export
   ;; ;; variables
   ;; #:*ping*
   ;; #:*ping-params*
   ;; #:*ssh*
   ;; #:*lisp-slave*
   ;; #:*lisp-user*

   ;; class
   :node
   :node-ssh
   :node-zmq

   ;; functions
   :node-name
   :node-alive-p
   :node-connect
   :node-disconnect
   :node-send
   :node-recv
   :node-flush				; ??
   :node-exec

   :node-lock

   ;; macroses
   :with-remote
   :rplet))
