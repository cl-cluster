;; Copyright 2010 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Cluster
;;
;; CL-Cluster is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Cluster is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :cl-cluster)

(defvar *context* (zmq:init 2))		; FIXME: ask for actual CPUs nr

(defclass node-zmq (node)
  ((endpoint :initarg :endpoint :accessor node-zmq-endpoint)
   (socket  :initform nil :accessor node-zmq-socket)
   (type    :initform zmq:req :initarg :type :accessor node-zmq-type)
   (message :initform nil :accessor node-zmq-message)))

(defmethod print-object ((object node-zmq) stream)
  (format stream "#N<NODE:\"~a\" ENDPOINT:\"~a\">"
	  (node-name object) (node-zmq-endpoint object)))

(defmethod node-alive-p ((object node-zmq))
  (not (null (node-zmq-socket object))))

(defmethod node-connect ((object node-zmq))
  (with-slots (socket endpoint type) object
    (setf socket (zmq:socket *context* type))
    (zmq:connect socket endpoint)))

(defmethod node-disconnect ((object node-zmq))
  (with-slots (socket) object
    (zmq:close socket)
    (setf socket nil)))

(defmethod node-send/unsafe ((object node-zmq) msg)
  (zmq:send (node-zmq-socket object) (make-instance 'zmq:msg :data msg)))

(defmethod node-recv/unsafe ((object node-zmq) &optional non-blocking)
  (let ((msg (make-instance 'zmq:msg)))
    (zmq:recv (node-zmq-socket object) msg)
    (read-from-string (zmq:msg-data-as-string msg))))

(defmethod node-flush/unsafe ((object node-zmq) &optional wait-input)
  t)
;  (zmq:flush (node-zmq-socket object)))
