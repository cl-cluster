;; multicast requires root
(asdf:oos 'asdf:load-op :zeromq)

(defpackage :cl-cluster-server
  (:use :cl))

(in-package :cl-cluster-server)

(defvar *feed-address* "epgm://lo;226.0.0.1:5555")
(defvar *repl-address* "tcp://127.0.0.1:5555")
(defvar *generation* 0)
(defvar *reqs* nil)

(zmq:with-context (ctx 2)
  (zmq:with-socket (feed ctx zmq:sub)
    (zmq:connect feed *feed-address*)
    (zmq:setsockopt feed zmq:subscribe "")
    (zmq:with-socket (repl ctx zmq:rep)
      (zmq:bind repl *repl-address*)
      (let ((msg (make-instance 'zmq:msg)))
	(loop
	   (progn
	     (format t "~%waiting for message~%")
	     (zmq:with-polls ((polls . ((feed . zmq:pollin)
					(repl . zmq:pollin))))
	       (let ((ret (zmq:poll polls))
		     req rep)
		 (format t "~a~%" ret)

		 (cond
		   ((= (car ret) zmq:pollin)
		    (zmq:recv feed msg)
		    (setq req (zmq:msg-data-as-string msg))
		    (format t "new feed: ~s~%" req)
		    (handler-case
			(progn
			  (eval (read-from-string req))
			  (incf *generation*)
			  (push req *reqs*))
		      (error (c)
			(format t "error: ~a~%" c))))
		   ((= (cadr ret) zmq:pollin)
		    (zmq:recv repl msg)
		    (setq req (zmq:msg-data-as-string msg))
		    (format t "->: ~s~%" req)
		    (setq rep (format nil "~s"
				      (handler-case
					  (eval (read-from-string req))
					(error (c)
					  (list 'error (format nil "~a" c))))))
		    (format t "<- ~s~%" rep)
		    (zmq:send repl (make-instance 'zmq:msg :data rep))))))))))
    (sleep 1)))

(tg:gc)
#+sbcl (sb-ext:quit)
#+clisp (ext:quit)

;
