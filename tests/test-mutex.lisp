(require :cl-cluster)

(in-package :cl-cluster)

(defparameter n1 (make-instance 'node :host "rhel4" :connect t))

(defun foo (x)
  (dotimes (i 1000)
    (let ((ans (node-exec n1 (format nil "~a" x))))
      (when (= (mod i 100) 0)
	(format t "~a[~a]: ~a~%" x i ans)
	(sb-thread:thread-yield))
      (unless (= x ans)
	(error (format nil "~a[~a]: ~a doesn't match" x i ans))))))

(dolist (i (mapcar (lambda (x)
		     (format t "start thread ~a~%" x)
		     (sb-thread:make-thread (lambda () (foo x))))
		   '(1 2 3 4)))
  (sb-thread:join-thread i))

(sb-ext:quit)
