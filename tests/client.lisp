(require :cl-cluster)

(in-package :cl-cluster)

(defvar my-remote (make-instance 'node-zmq :endpoint "tcp://127.0.0.1:5555"
				 :connect t))

(defvar feed (make-instance 'node-zmq
			    :endpoint "epgm://lo;226.0.0.1:5555"
			    :connect t :type zmq:pub))

(node-send feed "(defvar remote-list '(1 2 3 4 5 6 7 8))")

(format t "~a~%"
	(exec-remote my-remote
		     (loop for i in remote-list
			when (oddp i) collect i)))

(sb-ext:quit)
