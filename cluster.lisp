;; Copyright 2009, 2010 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Cluster
;;
;; CL-Cluster is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Cluster is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :cl-cluster)

(defclass node ()
  ((name :initform nil :initarg :name :accessor node-name)
   (type :initform nil :initarg :type :accessor node-type)
   (sexp :initform nil :accessor node-sexp)
   (lock :initform (bt:make-lock "node lock") :accessor node-lock)))

(defmethod print-object ((object node) stream)
  (format stream "#N<NODE:\"~a\">" (node-host object)))

(defgeneric node-alive-p (object)
  (:documentation "Returns T if node is alive."))

(defgeneric node-connect (object)
  (:documentation "Establish connection with remote host."))

(defmethod initialize-instance :after ((object node) &key connect &allow-other-keys)
  (when connect
    (node-connect object)))

(defgeneric node-disconnect (object)
  (:documentation "Close connection with remote host."))

(defgeneric node-send/unsafe (object msg)
  (:documentation "Send command to remote host."))

(defgeneric node-send (object msg)
  (:documentation "Send command to remote host (thread-safe)."))

(defmethod node-send (object msg)
  (when (not (node-alive-p object))
    (error (format nil "Node ~a not connected" object)))
  (bt:with-lock-held ((node-lock object))
    (node-send/unsafe object msg)))

(defgeneric node-recv/unsafe (object &optional non-blocking)
  (:documentation "Receive data from remote host.
If optional argument `non-blocking' is set, don't wait for
data."))

(defgeneric node-recv (object &optional non-blocking)
  (:documentation "Receive data from remote host (thread-safe).
If optional argument `non-blocking' is set, don't wait for
data."))

(defmethod node-recv (object &optional non-blocking)
  (when (not (node-alive-p object))
    (error (format nil "Node ~a not connected" object)))
  (bt:with-lock-held ((node-lock object))
    (node-recv/unsafe object non-blocking)))

(defgeneric node-flush/unsafe (object &optional wait-input)
  (:documentation "Flush available input data.
Wait for input when optional argument `wait-input' is set.
This is useful to skip interactive prompt."))

(defgeneric node-flush (object &optional wait-input)
  (:documentation "Flush available input data (thread-safe).
Wait for input when optional argument `wait-input' is set.
This is useful to skip interactive prompt."))

(defmethod node-flush (object &optional wait-input)
  (when (not (node-alive-p object))
    (error (format nil "Node ~a not connected" object)))
  (bt:with-lock-held ((node-lock object))
    (node-flush/unsafe object wait-input)))

(defgeneric node-exec (object cmd &optional guard trap-errors read-answer)
  (:documentation "Execute command on remote host and return result."))

(defmethod node-exec (object cmd &optional (guard t) (trap-errors t) (read-answer t))
  (declare (type string cmd))
  (bt:with-lock-held ((node-lock object))
    (when (not (node-alive-p object))
      (error (format nil "Node ~a not connected" object)))
    (node-flush/unsafe object)
    (setf (node-sexp object) cmd)
    (when guard
      (setq cmd (concatenate 'string
			     (format nil "(handler-case (eval (read-from-string ~s))" cmd)
			     "(error (condition) (list 'error (format nil \"~a\" condition))))")))
      (node-send/unsafe object cmd)
      (when read-answer
	(let ((answer (node-recv/unsafe object)))
	  (when (and (listp answer) trap-errors (eq (car answer) 'error))
	    (error "Error: ~a~%In form: ~a~%At node: ~a~%" (cadr answer) (node-sexp object) object))
	  (node-flush/unsafe object t)	; kill prompt
	  answer))))
