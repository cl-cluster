;; Copyright 2009, 2010 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Cluster
;;
;; CL-Cluster is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Cluster is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; (defpackage :cl-cluster-ssh
;;   (:use :cl :cl-cluster)
;;   (:export
;;    :node-ssh))

(in-package :cl-cluster)

(defparameter *ping*
  #+linux "/bin/ping"
  #+bsd "/sbin/ping")

(defparameter *ping-params*
  #+linux "-c 1 -w 1"
  #+bsd "-c 1 -W 1")

(defparameter *ssh* "/usr/bin/ssh")
(defparameter *lisp-slave* "/usr/bin/sbcl --noinform --core core")
(defparameter *lisp-user* "lisp")

(defun system (cmd args)
  (sb-ext:process-exit-code
   (sb-ext:run-program cmd (split-sequence:split-sequence #\Space args))))

(defun remote (cmd args)
  (sb-ext:run-program cmd (split-sequence:split-sequence #\Space args)
		      :input :stream :output :stream :wait nil))

(defclass node-ssh (node)
  ((host :initarg :host :accessor node-host)
   (lisp :initarg :lisp :initform *lisp-slave* :accessor node-lisp)
   (process :initform nil :accessor node-process)
   (input :initform nil :accessor node-input)
   (output :initform nil :accessor node-output)))

(defmethod print-object ((object node-ssh) stream)
  (format stream "#N<NODE: \"~a\" HOST:\"~a\" LISP:\"~a\">"
	  (node-name object) (node-host object) (node-lisp object)))

(defmethod node-alive-p ((object node-ssh))
  (with-slots (process) object
    (and process (sb-ext:process-p process) (sb-ext:process-alive-p process))))

(defmethod node-connect ((object node-ssh))
  (bt:with-lock-held ((node-lock object))
    (with-slots (host lisp process input output) object
      (when (not (sb-ext:process-p process))
	(when (= 0 (system *ping* (format nil "~a ~a" *ping-params* host)))
	  (setq process
		(remote *ssh* (format nil "-l ~a ~a ~a"
				      *lisp-user* host lisp))
		input (sb-ext:process-input process)
		output (sb-ext:process-output process))
	  (let ((proc process))
	    (tg:finalize object
			 (lambda () ;(format t "finalize ~a~%" proc)
			   (when proc (sb-ext:process-close proc))))))
	(node-flush/unsafe object t)	      ; discard prompt and other trash
	process))))

(defmethod node-disconnect ((object node-ssh))
  (bt:with-lock-held ((node-lock object))
    (with-slots (host lisp process input output) object
      (when (node-alive-p object)
	(node-send/unsafe object "(quit)")
	(close input)
	(close output)
	(sb-ext:process-kill process 9)))))

(defmethod node-send/unsafe ((object node-ssh) msg)
  (with-slots (input) object
    (princ msg input)
    (princ #\Newline input)
    (force-output input)))

(defmethod node-recv/unsafe ((object node-ssh) &optional non-blocking)
  (with-slots (output) object
    (if non-blocking
	(and (listen output)
	     (read output))
	(read output))))

(defmethod node-flush/unsafe ((object node-ssh) &optional wait-input)
  (when wait-input
    (with-slots (output) object
      (let ((timeout 60.0))
	(loop (when (or (listen output) (< (decf timeout 0.25) 0)) (return))
	   (sleep 0.25)))))
  (clear-input (node-output object)))
