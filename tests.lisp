(require :cl-cluster)

(in-package :cl-cluster)

;;;  tests
(let ((node1 (make-instance 'node :host "rhel4" :connect t))
      (node2 (make-instance 'node :host "rhel5" :connect t))
      (node3 (make-instance 'node :host "rhel5" :lisp "/usr/bin/clisp -q" :connect t)))
;  (declare (special node1 node2 node3))

  (dolist (node (list node1 node2 node3))
    (when (node-alive-p node)
      (handler-case
	  (progn
	      (format t "Remote \"~A\" runs ~A ~A~%"
		      (node-host node)
		      (node-exec node "(lisp-implementation-type)")
		      (node-exec node "(lisp-implementation-version)"))
	      (node-exec node "(killallhumans)"))
	(error (condition)
	  (format t "got error: ~A~%" condition)))))

  (print (rplet node3
	     ((a (with-remote node1
		   (+ 1 2)))
	      (b (with-remote node2
		   (sqrt 7d0))))
	   (* a b))))

(setq n1 (make-instance 'node :host "rhel4" :connect t))
(setq n2 (make-instance 'node :host "rhel5" :connect t))
;; connect to host's different lisp
(setq n3 (make-instance 'node :host "rhel5" :lisp "/usr/bin/clisp -q" :connect t))

(setq n4 (make-instance 'node :host "rhel4" :lisp "/usr/bin/clisp -q" :connect t))

;; node-exec expects only strings
(node-exec n1 1)

;; catch remote exception locally
(node-exec n1 "(lisp-implementation-type1)")

(with-remote n1
  (lisp-implementation-type)
  (lisp-implementation-version))

;; calculate some stuff on remote lisps and aggregate answers on another remote
(rplet n3
    ((a (with-remote n1
	  (+ 1 2)))
     (b (with-remote n2
	  (sqrt 7d0))))
  t
  (* a b))

(plet
    ((a (with-remote n1
	  (+ 1 2)))
     (b (with-remote n2
	  (sqrt 7d0))))
  t
  (* a b))

;; catch exception
(with-remote n1
  nil
  (dotimes (i 10)
    (+ i j)))

;; eval body in progn
(rplet n1 ()
  (lisp-implementation-type)
  (lisp-implementation-version))

;; avoid reader error
(with-remote n1
  (require 'asdf)
  (asdf:oos 'asdf:load-op :asdf-install))

(defun exit-from (n)
  (node-exec n "(save-lisp-and-die \"core\")")
  (node-disconnect n))

(setq n1 (make-instance 'node :host "rhel5" :connect t))

(with-remote n1
  (defvar a 1))

(with-remote n1
  (+ a 2))

(exit-from n1)


(require :cl-cluster)

(in-package :cl-cluster)

(setq my-remote (make-instance 'node-ssh :host "rhel5" :connect t))
(setq my-remote (make-instance 'node-zmq :endpoint "tcp://127.0.0.1:5555" :connect t))
(setq my-remote (make-instance 'node-zmq :endpoint "pgm://lo;226.0.0.1:5555" :connect t))

(node-exec my-remote "1")

(with-remote my-remote
  (defvar remote-list '(1 2 3 4 5 6 7 8)))

(with-remote my-remote "123")

(node-alive-p my-remote)

(node-disconnect my-remote)

(node-sexp my-remote)

(with-remote my-remote
  remote-list)

(with-remote my-remote
  (loop for i in remote-list
       when (oddp i) collect i))

(with-remote my-remote
  (unintern 'remote-list))

(with-remote my-remote
  remote-list)

(with-remote my-remote
  (defclass abc ()			; #<...> <- fails to read
    ((foo :initform 1))))

(with-remote my-remote
  (setq bar (make-instance 'abc)))

(with-remote my-remote
  (slot-value bar 'foo))

Error: The variable REMOTE-LIST is unbound.
In form: (PROGN REMOTE-LIST)
At node: #N<HOST:"rhel5" LISP:"/usr/bin/sbcl --noinform --core core">

   [Condition of type SIMPLE-ERROR]

;
